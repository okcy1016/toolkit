#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <confuse.h>
#include "opt.h"


#define TOOL_GOLDENDICT_HELPER 1
#define TOOL_XBACKLIGHT_HELPER 2
#define TOOL_DAEMON_MANAGER 3


yuck_t *parse_opt (int, char **);
void run_goldendict_helper (char *, bool);
void run_xbacklight_helper (char *);
cfg_t *parse_conf_file (char *conf_path);
void run_default_tools (char *toolkit_root_dir);
void killall_tools (char *toolkit_root_dir, bool is_output_disabled);
void configure (char conf_path[], yuck_t * argp, cfg_t * cfg,
		char **toolkit_root_dir);
void daemon_manager (char *toolkit_root_dir, int opa);


int
main (int argc, char *argv[])
{
  // todo: safety of the char array size overflow when strcat
  // todo: test `meson` and `cmake` as build system

  cfg_t *cfg = NULL;
  yuck_t *argp = parse_opt (argc, argv);
  int mode = 0;
  char conf_path[256];
  char *toolkit_root_dir = NULL;

  // configure from command line options and conf file
  configure (conf_path, argp, cfg, &toolkit_root_dir);

  if (toolkit_root_dir == NULL)
    {
      puts ("toolkit root dir not configured yet");
      exit (1);
    }

  switch (argp->cmd)
    {
    case K_CMD_NONE:
      // general cmd options here
      yuck_auto_help (argp);
      break;
    default:
      yuck_auto_help (argp);
      break;
    case K_CMD_G:
      mode = TOOL_GOLDENDICT_HELPER;
      break;
    case K_CMD_X:
      mode = TOOL_XBACKLIGHT_HELPER;
      break;
    case K_CMD_D:
      mode = TOOL_DAEMON_MANAGER;
      break;
    }

  // use tool
  switch (mode)
    {
    case TOOL_GOLDENDICT_HELPER:
      run_goldendict_helper (toolkit_root_dir, argp->g.foreground_flag);
      break;
    case TOOL_XBACKLIGHT_HELPER:
      run_xbacklight_helper (toolkit_root_dir);
      break;
    case TOOL_DAEMON_MANAGER:
      if (argp->d.run_default_tools_flag)
	daemon_manager (toolkit_root_dir, 0);
      else if (argp->d.kill_all_tools_flag)
	daemon_manager (toolkit_root_dir, 1);
      else
	yuck_auto_help (argp);
      break;
    default:
      break;
    }

  yuck_free (argp);
  cfg_free (cfg);
  return 0;
}

yuck_t *
parse_opt (int argc, char *argv[])
{
  yuck_t *argp = malloc (sizeof (yuck_t));

  yuck_parse (argp, argc, argv);
  return argp;
}

void
run_goldendict_helper (char *toolkit_root_dir, bool is_foreground)
{
  char command_str[256];
  strcpy (command_str, toolkit_root_dir);
  strcat (command_str, "/goldendict_helper/goldendict_helper");

  if (is_foreground)
    {
      system (command_str);
    }
  else
    {
      puts ("forking goldendict helper ...");
      strcat (command_str, " &");
      /* daemon (1, 0); */
      system (command_str);
    }
}

cfg_t *
parse_conf_file (char *conf_path)
{
  cfg_opt_t opts[] = {
    CFG_STR ("root_dir", "/tmp/k", CFGF_NONE),
    CFG_END ()
  };
  cfg_t *cfg = malloc (sizeof (cfg_t));

  cfg = cfg_init (opts, CFGF_NONE);

  switch (cfg_parse (cfg, conf_path))
    {
    case CFG_FILE_ERROR:
      printf ("failed to load config file: %s\n", conf_path);
      return NULL;
      break;
    case CFG_PARSE_ERROR:
      printf ("failed to parse config file: %s\n", conf_path);
      return NULL;
      break;
    case CFG_SUCCESS:
      break;
    default:
      printf ("error occured with config file: %s\n", conf_path);
      return NULL;
      break;
    }
  return cfg;
}

void
run_xbacklight_helper (char *toolkit_root_dir)
{
  char command_str[256];
  strcpy (command_str, toolkit_root_dir);
  strcat (command_str, "/xbacklight_helper/xbacklight_helper");
  strcat (command_str, " &");

  puts ("forking xbacklight helper ...");
  /* daemon (1, 0); */
  system (command_str);
}

void
run_default_tools (char *toolkit_root_dir)
{
  killall_tools (toolkit_root_dir, true);
  run_goldendict_helper (toolkit_root_dir, false);
  run_xbacklight_helper (toolkit_root_dir);
}

void
killall_tools (char *toolkit_root_dir, bool is_output_disabled)
{
  char command_str[256];

  /* strcpy (command_str, "killall "); */
  /* strcat (command_str, toolkit_root_dir); */
  /* strcat (command_str, "/goldendict_helper/goldendict_helper"); */
  strcpy (command_str, "killall goldendict_helper xbacklight_helper");
  if (is_output_disabled)
    strcat (command_str, " > /dev/null 2>&1");
  system (command_str);
  command_str[0] = '\0';
}

void
configure (char conf_path[], yuck_t * argp, cfg_t * cfg,
	   char **toolkit_root_dir)
{

#ifdef TEST

  *toolkit_root_dir = "build";

#else

  // config file path
  strcpy (conf_path, getenv ("HOME"));
  strcat (conf_path, "/.config/k");

  // get config file path from command line    
  if (argp->conf_path_arg != NULL)
    {
      // clear array
      conf_path[0] = '\0';
      strcat (conf_path, argp->conf_path_arg);
    }

  // load conf file
  cfg = parse_conf_file (conf_path);
  if (cfg != NULL)
    {
      *toolkit_root_dir = cfg_getstr (cfg, "root_dir");
    }

#endif
}

void
daemon_manager (char *toolkit_root_dir, int opa)
{
  // operation definition
  // 0 --> run_default_tools
  // 1 --> kill_all_tools

  switch (opa)
    {
    default:
      break;
    case 0:
      run_default_tools (toolkit_root_dir);
      break;
    case 1:
      killall_tools (toolkit_root_dir, false);
      break;
    }
}
