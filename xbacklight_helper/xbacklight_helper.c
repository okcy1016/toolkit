// ---------------------------------- //
// edited from freedesktop xbacklight //
// ---------------------------------- //

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include <xcb/xcb.h>
#include <xcb/xcb_util.h>
#include <xcb/xproto.h>
#include <xcb/randr.h>
#include <xcb/xcb_keysyms.h>
#include <X11/keysym.h>

#include <ctype.h>
#include <string.h>
#include <unistd.h>


static xcb_atom_t backlight, backlight_new, backlight_legacy;


static void backlight_set (xcb_connection_t * conn, xcb_randr_output_t output,
			   long value);
static long backlight_get (xcb_connection_t * conn,
			   xcb_randr_output_t output);
int backlight_change_by_step (xcb_connection_t * conn, xcb_screen_t * screen,
			      double step_value);
int check_preqs (xcb_connection_t * conn);
void tune_backlight (xcb_connection_t * conn);


int
main (void)
{
  xcb_connection_t *conn = xcb_connect (NULL, NULL);
  if (check_preqs (conn) != 0)
    {
      exit (EXIT_FAILURE);
    }

  tune_backlight (conn);

  xcb_aux_sync (conn);
  return EXIT_SUCCESS;
}

void
tune_backlight (xcb_connection_t * conn)
{
  /* Get the first screen */
  xcb_screen_t *screen = xcb_setup_roots_iterator (xcb_get_setup (conn)).data;

  // grab j,k key --> inc, dec OPA
  xcb_key_symbols_t *keysyms = xcb_key_symbols_alloc (conn);
  xcb_keycode_t *keycode_j = xcb_key_symbols_get_keycode (keysyms, XK_j);
  xcb_keycode_t *keycode_k = xcb_key_symbols_get_keycode (keysyms, XK_k);
  xcb_keycode_t *keycode_q = xcb_key_symbols_get_keycode (keysyms, XK_q);
  xcb_grab_key (conn, 0, screen->root, XCB_MOD_MASK_1, *keycode_j,
		XCB_GRAB_MODE_ASYNC, XCB_GRAB_MODE_ASYNC);
  xcb_grab_key (conn, 0, screen->root, XCB_MOD_MASK_1, *keycode_k,
		XCB_GRAB_MODE_ASYNC, XCB_GRAB_MODE_ASYNC);
  xcb_grab_key (conn, 0, screen->root, XCB_MOD_MASK_1, *keycode_q,
		XCB_GRAB_MODE_ASYNC, XCB_GRAB_MODE_ASYNC);
  xcb_flush (conn);

  xcb_generic_event_t *ev;
  while (ev = xcb_wait_for_event (conn))
    {
      switch (ev->response_type & ~0x80)
	{
	case XCB_KEY_PRESS:
	  {
	    // keycode 44 -> j, 45 -> k, 24 -> q
	    xcb_key_press_event_t *kp = (xcb_key_press_event_t *) ev;
	    /* printf("%d\n", kp->detail); */
	    if (kp->detail == 44)
	      {
		backlight_change_by_step (conn, screen, -0.5);
	      }
	    else if (kp->detail == 45)
	      {
		backlight_change_by_step (conn, screen, 0.5);
	      }
	    else
	      {
		xcb_ungrab_key (conn, *keycode_j, screen->root,
				XCB_MOD_MASK_1);
		xcb_ungrab_key (conn, *keycode_k, screen->root,
				XCB_MOD_MASK_1);
		free (ev);
		exit (EXIT_SUCCESS);
	      }
	    break;
	  }
	default:
	  {
	    break;
	  }
	}
    }
  free (ev);
}

int
backlight_change_by_step (xcb_connection_t * conn, xcb_screen_t * screen,
			  double step_value)
{
  xcb_generic_error_t *error;
  double new, min, max, set, cur;
  xcb_window_t root = screen->root;
  xcb_randr_output_t *outputs;
  xcb_randr_get_screen_resources_current_cookie_t resources_cookie;
  xcb_randr_get_screen_resources_current_reply_t *resources_reply;

  resources_cookie = xcb_randr_get_screen_resources_current (conn, root);
  resources_reply =
    xcb_randr_get_screen_resources_current_reply (conn, resources_cookie,
						  &error);
  if (error != NULL || resources_reply == NULL)
    {
      int ec = error ? error->error_code : -1;
      fprintf (stderr, "RANDR Get Screen Resources returned error %d\n", ec);
      exit (1);
    }

  outputs = xcb_randr_get_screen_resources_current_outputs (resources_reply);
  for (int o = 0; o < resources_reply->num_outputs; o++)
    {
      xcb_randr_output_t output = outputs[o];

      cur = backlight_get (conn, output);
      if (cur != -1)
	{
	  // check preqs
	  xcb_randr_query_output_property_cookie_t prop_cookie;
	  xcb_randr_query_output_property_reply_t *prop_reply;

	  prop_cookie =
	    xcb_randr_query_output_property (conn, output, backlight);
	  prop_reply =
	    xcb_randr_query_output_property_reply (conn, prop_cookie, &error);

	  if (error != NULL || prop_reply == NULL)
	    continue;

	  if (prop_reply->range &&
	      xcb_randr_query_output_property_valid_values_length
	      (prop_reply) == 2)
	    {
	      int32_t *values =
		xcb_randr_query_output_property_valid_values (prop_reply);
	      min = values[0];
	      max = values[1];

	      /* printf ("previous: %f\n", (cur - min) * 100 / (max - min)); */

	      set = step_value * (max - min) / 100;
	      new = cur + set;
	      if (new > max)
		new = max;
	      if (new < min)
		new = min;
	      backlight_set (conn, output, (long) (new + 0.5));
	      xcb_flush (conn);
	      /* usleep (200 * 1000 / steps); // 200ms */
	    }
	  free (prop_reply);
	}
    }
  free (resources_reply);
}

static long
backlight_get (xcb_connection_t * conn, xcb_randr_output_t output)
{
  xcb_generic_error_t *error;
  xcb_randr_get_output_property_reply_t *prop_reply = NULL;
  xcb_randr_get_output_property_cookie_t prop_cookie;
  long value;

  backlight = backlight_new;
  if (backlight != XCB_ATOM_NONE)
    {
      prop_cookie = xcb_randr_get_output_property (conn, output,
						   backlight, XCB_ATOM_NONE,
						   0, 4, 0, 0);
      prop_reply =
	xcb_randr_get_output_property_reply (conn, prop_cookie, &error);
      if (error != NULL || prop_reply == NULL)
	{
	  backlight = backlight_legacy;
	  if (backlight != XCB_ATOM_NONE)
	    {
	      prop_cookie = xcb_randr_get_output_property (conn, output,
							   backlight,
							   XCB_ATOM_NONE, 0,
							   4, 0, 0);
	      prop_reply =
		xcb_randr_get_output_property_reply (conn, prop_cookie,
						     &error);
	      if (error != NULL || prop_reply == NULL)
		{
		  return -1;
		}
	    }
	}
    }

  if (prop_reply == NULL ||
      prop_reply->type != XCB_ATOM_INTEGER ||
      prop_reply->num_items != 1 || prop_reply->format != 32)
    {
      value = -1;
    }
  else
    {
      value = *((int32_t *) xcb_randr_get_output_property_data (prop_reply));
    }

  free (prop_reply);
  return value;
}

static void
backlight_set (xcb_connection_t * conn, xcb_randr_output_t output, long value)
{
  xcb_randr_change_output_property (conn, output, backlight, XCB_ATOM_INTEGER,
				    32, XCB_PROP_MODE_REPLACE,
				    1, (unsigned char *) &value);
}


int
check_preqs (xcb_connection_t * conn)
{
  xcb_generic_error_t *error;

  xcb_randr_query_version_cookie_t ver_cookie;
  xcb_randr_query_version_reply_t *ver_reply;

  xcb_intern_atom_cookie_t backlight_cookie[2];
  xcb_intern_atom_reply_t *backlight_reply;

  // check sth
  ver_cookie = xcb_randr_query_version (conn, 1, 2);
  ver_reply = xcb_randr_query_version_reply (conn, ver_cookie, &error);
  if (error != NULL || ver_reply == NULL)
    {
      int ec = error ? error->error_code : -1;
      fprintf (stderr, "RANDR Query Version returned error %d\n", ec);
      exit (1);
    }
  if (ver_reply->major_version != 1 || ver_reply->minor_version < 2)
    {
      fprintf (stderr, "RandR version %d.%d too old\n",
	       ver_reply->major_version, ver_reply->minor_version);
      exit (1);
    }
  free (ver_reply);

  backlight_cookie[0] =
    xcb_intern_atom (conn, 1, strlen ("Backlight"), "Backlight");
  backlight_cookie[1] =
    xcb_intern_atom (conn, 1, strlen ("BACKLIGHT"), "BACKLIGHT");

  backlight_reply = xcb_intern_atom_reply (conn, backlight_cookie[0], &error);
  if (error != NULL || backlight_reply == NULL)
    {
      int ec = error ? error->error_code : -1;
      fprintf (stderr, "Intern Atom returned error %d\n", ec);
      exit (1);
    }

  backlight_new = backlight_reply->atom;
  free (backlight_reply);

  backlight_reply = xcb_intern_atom_reply (conn, backlight_cookie[1], &error);
  if (error != NULL || backlight_reply == NULL)
    {
      int ec = error ? error->error_code : -1;
      fprintf (stderr, "Intern Atom returned error %d\n", ec);
      exit (1);
    }

  backlight_legacy = backlight_reply->atom;
  free (backlight_reply);

  if (backlight_new == XCB_NONE && backlight_legacy == XCB_NONE)
    {
      fprintf (stderr, "No outputs have backlight property\n");
      exit (1);
    }

  return 0;
}
