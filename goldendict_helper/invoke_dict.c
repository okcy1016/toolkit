#include "helper.h"


int
_invoke_dict (void)
{
  // connect x
  Display *dpy = XOpenDisplay (NULL);
  if (dpy == NULL)
    {
      puts ("XOpenDisplay error");
      return -1;
    }

  // send keystroke ctrl+f12
  /* on my system, key code: */
  /* 37 -> ctrl */
  /* 54 -> c */
  /* 96 -> f12 */
  /* key type: */
  /* 2 -> key_press */
  /* 3 -> key_release */
  XTestFakeKeyEvent (dpy, 37, true, 0);
  XTestFakeKeyEvent (dpy, 96, true, 0);
  XTestFakeKeyEvent (dpy, 96, false, 0);
  XTestFakeKeyEvent (dpy, 37, false, 0);
  XSync (dpy, true);

  // disconnect
  XCloseDisplay (dpy);
  return EXIT_SUCCESS;
}
