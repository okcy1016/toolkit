#include "helper.h"

/* (Linux X11 only) */
/* 这个文件的作用是捕捉 ctrl+c ctrl+c 信号然后根据当前所聚焦的窗口来做不同的操作： */
/* 1. 若当前窗口为 urxvt terminal，不做任何操作。 */
/* 2. 为其他窗口时向 root windows 发送 ctrl+F12 信号触发 Goldendict 快捷翻译。 */


int
main (void)
{
  enable_record_xevents ();
  return EXIT_SUCCESS;
}

int
enable_record_xevents (void)
{
  Display *dpy = NULL;
  struct xevent_callback_params xevent_callback_param_struct = {.state =
      0,.p2dpy = &dpy
  };

  dpy = XOpenDisplay (NULL);
  /* printf("dpy: %d\n", dpy); */
  if (dpy == NULL)
    {
      fprintf (stderr, "XOpenDisplay error.\n");
      return -1;
    }

  XRecordRange *record_range = XRecordAllocRange ();
  if (record_range == NULL)
    {
      fprintf (stderr, "XRecordAllocRange error.\n");
      return -1;
    }

  record_range->device_events.first = KeyPress;
  record_range->device_events.last = KeyRelease;
  XRecordClientSpec client_spec = XRecordAllClients;

  XRecordContext context =
    XRecordCreateContext (dpy, 0, &client_spec, 1, &record_range, 1);
  if (context == 0)
    {
      fprintf (stderr, "XRecordCreateContext error.\n");
      return -1;
    }
  XSync (dpy, true);
  XFree (record_range);

  if (XRecordEnableContext
      (dpy, context, xevent_callback,
       (XPointer) & xevent_callback_param_struct) == 0)
    {
      fprintf (stderr, "XRecordEnableContext error.\n");
      return -1;
    }

  return 0;
}


void
xevent_callback (XPointer p_xevent_callback_param_struct,
		 XRecordInterceptData * d)
{
  handle_xrecordevent ((struct xevent_callback_params *)
		       p_xevent_callback_param_struct, d);
}

void
invoke_dict (struct xevent_callback_params *pd)
{
  // 判断当前窗口是不是 urxvt terminal
  bool is_firefox = identify_focus_win ();
  /* printf("%d\n", is_urxvt); */
  if (is_firefox)
    _invoke_dict ();
}

void
handle_xrecordevent (struct xevent_callback_params *pd,
		     XRecordInterceptData * d)
{
  // on my system, key code:
  // 37 -> ctrl
  // 54 -> c
  // 96 -> f12
  // key type:
  // 2 -> key_press
  // 3 -> key_release

  if (d->category == XRecordFromServer)
    {
      int xev_key_code = ((unsigned char *) d->data)[1];
      int xev_key_type = ((unsigned char *) d->data)[0] & 0x7f;
      // int repeat = d->data[2] & 1;
      /* printf("key: %d\n", xev_key_code); */
      /* printf("type: %d\n", xev_key_type); */

      // detect ctrl+c ctrl+c using state machine
      switch (pd->state)
	{
	case 0:
	  // detect ctrl press
	  if (xev_key_code == 37 && xev_key_type == 2)
	    {
	      pd->state = 1;
	    }
	  else
	    pd->state = 0;
	  break;
	case 1:
	  // detect c press
	  if (xev_key_code == 54 && xev_key_type == 2)
	    {
	      pd->state = 2;
	    }
	  else
	    pd->state = 0;
	  break;
	case 2:
	  // detect c release
	  if (xev_key_code == 54 && xev_key_type == 3)
	    {
	      pd->state = 3;
	    }
	  else
	    pd->state = 0;
	  break;
	case 3:
	  // detect c press
	  if (xev_key_code == 54 && xev_key_type == 2)
	    invoke_dict (pd);
	  pd->state = 0;
	  break;
	default:
	  break;
	}
    }
  XRecordFreeData (d);
}
