#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <X11/Xlib.h>
#include <X11/extensions/record.h>
#include <X11/extensions/XTest.h>


struct xevent_callback_params
{
  int state;
  Display **p2dpy;
};

void goldendict_helper_run (void);
void xevent_callback (XPointer, XRecordInterceptData *);
void handle_xrecordevent (struct xevent_callback_params *,
			  XRecordInterceptData *);
int enable_record_xevents (void);
void invoke_dict (struct xevent_callback_params *);

bool start_with (const char *pre, const char *str);
bool identify_focus_win (void);
int _invoke_dict (void);
void check_xstatus (int status, unsigned long window);
unsigned char *get_string_property (Display * display, unsigned long window,
				    char *property_name);
unsigned long get_long_property (Display * display, unsigned long window,
				 char *property_name);
