#include "helper.h"


bool
identify_focus_win (void)
{
  // connect x
  Display *dpy = XOpenDisplay (NULL);
  if (dpy == NULL)
    {
      puts ("XOpenDisplay error");
      return -1;
    }

  int screen = XDefaultScreen (dpy);
  unsigned long root_win = RootWindow (dpy, screen);
  unsigned long active_win =
    get_long_property (dpy, root_win, "_NET_ACTIVE_WINDOW");
  unsigned char *active_win_name =
    get_string_property (dpy, active_win, "_NET_WM_NAME");

  bool is_firefox = false;
  if (strstr (active_win_name, "Mozilla Firefox") != NULL)
    {
      // contains
      is_firefox = true;
    }

  XCloseDisplay (dpy);
  return is_firefox;
}

bool
start_with (const char *pre, const char *str)
{
  if (pre == NULL || str == NULL)
    return false;
  return strncmp (pre, str, strlen (pre)) == 0;
}


// {
// brought from https://github.com/UltimateHackingKeyboard/current-window-linux
void
check_xstatus (int status, unsigned long window)
{
  if (status == BadWindow)
    {
      printf ("window id # 0x%lx does not exists!", window);
      exit (1);
    }

  if (status != Success)
    {
      printf ("XGetWindowProperty failed!");
      exit (2);
    }
}

unsigned char *
get_string_property (Display * display, unsigned long window,
		     char *property_name)
{
  Atom actual_type, filter_atom;
  int actual_format, status;
  unsigned long nitems, bytes_after;
  unsigned char *prop;
  int MAXSTR = 1000;

  filter_atom = XInternAtom (display, property_name, True);
  status =
    XGetWindowProperty (display, window, filter_atom, 0, MAXSTR, False,
			AnyPropertyType, &actual_type, &actual_format,
			&nitems, &bytes_after, &prop);
  check_xstatus (status, window);
  return prop;
}

unsigned long
get_long_property (Display * display, unsigned long window,
		   char *property_name)
{
  unsigned char *prop = get_string_property (display, window, property_name);
  unsigned long long_property =
    prop[0] + (prop[1] << 8) + (prop[2] << 16) + (prop[3] << 24);
  return long_property;
}

// }
